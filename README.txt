Esse é um projeto para a cadeira de Experimentação de Protótipo da faculdade de Ciência da Computação na UNIFOR.
Ele consiste da criação de um jogo eletrônico utilizando a Engine GODOT.
O objetivo do projeto foi a criação de um jogo em um período curto, que utilizasse temas escolhidos randomicamente por uma API e que tivesse interação com um controle também criado pela equipe utilizando o ESP32.
Eu fui responsável pela criação do jogo enquanto o outro membro da equipe se responsabilizou pela criação do controle.
Arquivo deve ser aberto na Engine Godot e para executar deve-se clicar no F6. Os botões para jogar são os direcionais WASD e E, R e F.
Vídeo apresentação do projeto: https://youtu.be/d-gy0pC-9m8

Por Gilberto Egypto

