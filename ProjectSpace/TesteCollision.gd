extends CollisionShape2D

var acrescimo = 0
const SHAPE_RADIUS = 59
var luz = 0

func _receive_Signal(sinal):
	if sinal < 1 :
		acrescimo = 0
	elif sinal > 1 and sinal < 2:
		acrescimo = 10
	
	self.shape.radius = SHAPE_RADIUS + acrescimo + luz
	
