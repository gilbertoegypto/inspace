extends KinematicBody2D

export var walk_speed = 250
export var movement = Vector2()
export var facingDir = Vector2()
export var interactDist = 70
export var flashLight = true
export var KeyA = false
export var KeyB = false
export var KeyC = false
export var KeyD = false
export var unlock = false
export var hologram = 20
signal interaction(command)
signal keyCollected(key)
signal unlocked(lock)
signal hologram(location)
signal death(i)
export var flare = 20
signal flare(location, facingdir)
signal flareDialogue(yorn)
signal hologramCollected(i)


func _physics_process(delta):
	var collision = move_and_collide(movement*delta)
	if collision:
		if collision.collider.is_in_group("inimigo"):
			emit_signal("death",1)
			self.visible = false
			yield(get_tree().create_timer(3.0),"timeout")
			get_tree().reload_current_scene()
		if collision.collider.is_in_group("inimigo_dia"):
			emit_signal("death",1)
			self.visible = false
			yield(get_tree().create_timer(3.0),"timeout")
			get_tree().reload_current_scene()
		
	movement = Vector2()
	#Input
	if Input.is_action_pressed("DOWN"):
		movement.y+=1
		facingDir = Vector2(0,-1)
	if Input.is_action_pressed("UP"):
		movement.y+=-1
		facingDir = Vector2(0,1)
	if Input.is_action_pressed("LEFT"):
		movement.x+=-1
		facingDir = Vector2(-1,0)
	if Input.is_action_pressed("RIGHT"):
		movement.x+=1
		facingDir = Vector2(1,0)
	if Input.is_action_just_pressed("FLASHLIGHT") and flashLight == true:
		$Light2D_outer.enabled = not $Light2D_outer.enabled
		$Light2d_inner.enabled = not $Light2d_inner.enabled
		yield(get_tree().create_timer(3.1),"timeout")
		_flash_light()
		$Light2D_outer.enabled = not $Light2D_outer.enabled
		$Light2d_inner.enabled = not $Light2d_inner.enabled
	elif Input.is_action_just_pressed("FLASHLIGHT") and flashLight == false:
		print("You can't do that now!")
	if Input.is_action_just_pressed("INTERACT") and hologram > 0:
		var local = Vector2(self.position)
		print(local)
		emit_signal("hologram",local)
		hologram-=1
	if Input.is_action_just_pressed("ITEM") and flare >0:
		var local = Vector2(self.position)
		emit_signal("flare", local, facingDir)
		flare-=1
	_changing_sprites()
	movement=movement.normalized()
	#move the player
	move_and_slide(movement*walk_speed)
	
	
func _changing_sprites():
	if facingDir.y < 0:
		$AnimatedSprite.play("idleFront")
	if facingDir.y > 0:
		$AnimatedSprite.play("idleBack")
	if facingDir.x!=0:
		$AnimatedSprite.play("idleSide")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )
		
func _flash_light():
	flashLight = false
	yield(get_tree().create_timer(10.0),"timeout")
	flashLight = true


func _on_Event_Area_Cafeteria_body_entered(body):
	if KeyB == true && KeyA == true && KeyC == true && KeyD == true: 
		emit_signal("interaction", "allKey")
	else:
		emit_signal("interaction","noKey")



func _on_Event_Area_Lab_body_entered(body):
	if KeyB == false:
		KeyB = true
		emit_signal("keyCollected", "B")
	elif KeyB == true:
		pass


func _on_Event_Area_Maintanance_body_entered(body):
	if KeyA == false:
		KeyA = true
		emit_signal("keyCollected", "A")
	elif KeyA == true:
		pass


func _on_Event_Area_Exit_body_entered(body):
	if unlock == false:
		emit_signal("unlocked","no")
	if unlock == true:
		emit_signal("unlocked","yes")


func _on_DialogoText_lightsOn(on):
	unlock = true


func _on_Event_Area_ComputerSecurity_body_entered(body):
	if KeyC == false:
		KeyC = true
		emit_signal("keyCollected", "C")
	elif KeyC == true:
		pass


func _on_Event_Area_1ServerRoom2_body_entered(body):
	if KeyD == false:
		KeyD = true
		emit_signal("keyCollected", "D")
	elif KeyD == true:
		pass


func _on_Event_Area_Corridor_Death_body_entered(body):
	self.visible = false
	yield(get_tree().create_timer(3.0),"timeout")
	get_tree().reload_current_scene()



func _on_HologramItem1_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_HologramItem2_body_entered(body):
	hologram+=1
	emit_signal("hologramCollected",1)


func _on_Area2D_body_entered(body):
	flare+=1
	emit_signal("flareDialogue", true)
