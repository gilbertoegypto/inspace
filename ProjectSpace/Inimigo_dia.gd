extends KinematicBody2D
var player = null;
var move = Vector2.ZERO
var speed = 1
export var move_speed = 80
var ativo = false
var visivel = false


func _physics_process(delta):
	
	if ativo == true and visivel == true:
		self.visible = visivel
		move = Vector2.ZERO
		if player != null :
			move = position.direction_to(player.position*1) * speed
			
		else:
			move = Vector2.ZERO
	_changing_sprites()
	move = move.normalized()
	move_and_slide(move*move_speed)
	


func _on_Area2D_body_entered(body):
	if body != self and ativo == true:
		player = body


func _on_Area2D_body_exited(body):
	player = null
	pass # Replace with function body.




func _changing_sprites():
	if move.x < 0:
		$Sprite/EnemyAnimation/EnemyAnimationSprite.flip_h=true
	if move.x > 0:
		$Sprite/EnemyAnimation/EnemyAnimationSprite.flip_h=false


func _on_DialogoText_lightsOn(on):
	ativo = true
	visivel = true
	self.set_collision_mask(8)
	self.set_collision_layer(8)
