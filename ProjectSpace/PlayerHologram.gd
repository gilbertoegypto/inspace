extends KinematicBody2D


export var walk_speed = 100
export var movement = Vector2()
export var facingDir = Vector2()
export var interactDist = 70


#export var spawn.x = $player.position.x



func _physics_process(delta):
	movement = Vector2()
	#Input
	if Input.is_action_pressed("DOWN"):
		movement.y+=-1
		facingDir = Vector2(0,1)
	if Input.is_action_pressed("UP"):
		movement.y+=1
		facingDir = Vector2(0,-1)
	if Input.is_action_pressed("LEFT"):
		movement.x+=1
		facingDir = Vector2(1,0)
	if Input.is_action_pressed("RIGHT"):
		movement.x+=-1
		facingDir = Vector2(-1,0)
	_changing_sprites()
	movement=movement.normalized()
	#move the player
	move_and_slide(movement*walk_speed)
	
	
func _changing_sprites():
	if facingDir.y < 0:
		$AnimatedSprite.play("idleFront")
	if facingDir.y > 0:
		$AnimatedSprite.play("idleBack")
	if facingDir.x!=0:
		$AnimatedSprite.play("idleSide")
		if facingDir.x < 0:
			$AnimatedSprite.set_flip_h( false )
		if facingDir.x > 0:
			$AnimatedSprite.set_flip_h( true )


func _on_Player_hologram(location):
	print("Signal received")
	self.position = location
	print(location)
	self.visible = true
	yield(get_tree().create_timer(5.0),"timeout")
	self.visible = false
