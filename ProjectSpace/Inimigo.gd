extends KinematicBody2D
var player = null;
var move = Vector2.ZERO
var speed = 1
export var move_speed = 80
var seguir = true
func _ready():
	pass

func _physics_process(delta):
	move = Vector2.ZERO
	
	
	if Input.is_action_just_pressed("FLASHLIGHT") and seguir == true:
		#get_tree().reload_current_scene()
		#move = Vector2.ZERO
		_flash_light()
		
	elif Input.is_action_just_pressed("FLASHLIGHT") and seguir == false:
		print("")
	
	
	if player != null and seguir == true:
		move = position.direction_to(player.position*1) * speed
		
	else:
		move = Vector2.ZERO
		

	_changing_sprites()
	move = move.normalized()
	move_and_slide(move*move_speed)
	
	
	
func _on_Area2D_body_entered(body):
	if body != self:
		player = body


func _on_Area2D_body_exited(body):
	player = null
	

func _flash_light():
	seguir = false
	
	yield(get_tree().create_timer(3.1),"timeout")
	
	seguir = true
	yield(get_tree().create_timer(6.9),"timeout")


func _on_CollisionShape2D_sp(local):
	self.position = local
	
	pass # Replace with function body.

func _changing_sprites():
	if move.x < 0:
		$Sprite/EnemyAnimation/EnemyAnimationSprite.flip_h=true
	if move.x > 0:
		$Sprite/EnemyAnimation/EnemyAnimationSprite.flip_h=false


func _on_DialogoText_lightsOn(on):
	self.queue_free()

